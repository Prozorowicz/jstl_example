<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Tomasz
  Date: 02/10/2018
  Time: 18:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Product form</title>
</head>
<body>
<h2>Formularz produktu</h2>
<form action="product_details.jsp" method="get">
    <div>
        <label for="name">Name:</label>
        <input type="text" name="name" id="name">
    </div>
    <div>
        <label for="price">Price:</label>
        <input type="number" name="price" id="price" step="0.01">
    </div>
    <div>
        <label for="expires">Expires on:</label>
        <input type="date" name="expires" id="expires">
    </div>
    <div>
        <label for="amount">Amount:</label>
        <input type="number" name="amount" id="amount">
    </div>
    <div>
        <input type="submit" value="przeslij">
    </div>
</form>
</body>
</html>
