<%@ page import="com.jstl.Product" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.time.LocalDate" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Tomasz
  Date: 02/10/2018
  Time: 18:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Product details</title>
</head>
<body>
<h2>Product details:</h2>
<%
    if (request.getParameter("name") != null && request.getParameter("amount")
            != null && request.getParameter("price") != null && request.getParameter("expires") != null) {


        Product createdProduct = new Product();
        createdProduct.setName(request.getParameter("name"));
        createdProduct.setPrice(Double.parseDouble(request.getParameter("price")));
        createdProduct.setExpireDate(LocalDate.parse(request.getParameter("expires")));
        createdProduct.setAmount(Integer.parseInt(request.getParameter("amount")));
        pageContext.setAttribute("createdProduct", createdProduct);
    }
%>
<c:choose>
    <c:when test="${not empty createdProduct}">
        <table>
            <tr>
                <td>Name:</td>
                <td>
                    <c:out value="${createdProduct.name}"/>
                </td>
            </tr>
            <tr>
                <td>Price:</td>
                <td>
                    <c:out value="${createdProduct.price}"/>
                </td>
            </tr>
            <tr>
                <td>Expires:</td>
                <td>
                    <c:out value="${createdProduct.expireDate}"/>
                </td>
            </tr>
            <tr>
                <td>Amount:</td>
                <td>
                    <c:out value="${createdProduct.amount}"/>
                </td>
            </tr>
        </table>
    </c:when>
    <c:otherwise>
        <h3>Product details missing!</h3>
    </c:otherwise>
</c:choose>
</body>
</html>
