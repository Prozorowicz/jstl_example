<%@ page import="com.jstl.Student" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Iterator" %><%--
  Created by IntelliJ IDEA.
  User: Tomasz
  Date: 02/10/2018
  Time: 20:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Remove student</title>
</head>
<body>
<%@include file="header.jsp"%>
<%
    List<Student> students;
    if(session.getAttribute("student_list")!=null){
        students = (List<Student>) session.getAttribute("student_list");

    }else {
        students = new ArrayList<>();
    }
    String removeuserId = request.getParameter("id");
    int removedId = Integer.parseInt(removeuserId);
    Iterator<Student> it = students.iterator();
    while (it.hasNext()){
        Student student = it.next();
        if (student.getId()==removedId){
            it.remove();
            break;
        }
    }
    session.setAttribute("student_list",students);
    response.sendRedirect("student_list.jsp");
%>
</body>
</html>
