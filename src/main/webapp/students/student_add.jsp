<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="com.jstl.Student" %>
<%@ page import="java.time.LocalDate" %>
<%@ page import="java.time.format.DateTimeFormatter" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: Tomasz
  Date: 02/10/2018
  Time: 19:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Student Add</title>
</head>
<body>
<%@include file="header.jsp"%>
<%! int licznik = 0; %>
<%
    Student u = new Student();
    u.setId(licznik++);
    u.setName(request.getParameter("name"));
    u.setSurname(request.getParameter("surname"));
    u.setIndex(request.getParameter("index"));
    LocalDate birthdate = LocalDate.parse(request.getParameter("birthdate"), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    u.setBirthdate(birthdate);
    //////////////////////////////////////
    List<Student> students;
    if (session.getAttribute("student_list") != null) {
        students = (List<Student>) session.getAttribute("student_list");
    } else {
        students = new ArrayList<>();
    }
    students.add(u);
    session.setAttribute("student_list", students);
    response.sendRedirect("/students/student_list.jsp");
%>
</body>
</html>
