<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Tomasz
  Date: 02/10/2018
  Time: 19:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Student form</title>
</head>
<body>
<%@include file="header.jsp"%>
<h2>Formularz dodawania studenta</h2>
<form action="student_add.jsp">
    <div>
        <label for="name">Name</label>
        <input id="name" name="name" type="text">
    </div>
    <div>
        <label for="surname">Surname</label>
        <input id="surname" name="surname" type="text">
    </div>
    <div>
        <label for="index">index</label>
        <input id="index" name="index" type="text">
    </div>
    <div>
        <label for="birthdate">birthdate</label>
        <input id="birthdate" name="birthdate" type="date">
    </div>
    <input type="submit" value="Dodaj">
</form>
</body>
</html>
