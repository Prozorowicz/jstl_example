<%@ page import="com.jstl.Student" %>
<%@ page import="java.util.List" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Tomasz
  Date: 02/10/2018
  Time: 19:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Student List</title>
</head>
<body>
<%@include file="header.jsp"%>


<h2>Lista Studentow</h2>
<table>
    <thead>
    <th>id</th>
    <th>name</th>
    <th>surname</th>
    <th>index</th>
    <th>data urodzenia</th>
    <th>actions</th>
    </thead>
    <tbody>
    <c:forEach var="student" items="${sessionScope.student_list}">
        <tr>
            <td>
                <c:out value="${student.id}"/>
            </td>
            <td>
                <c:out value="${student.name}"/>
            </td>
            <td>
                <c:out value="${student.surname}"/>
            </td>
            <td>
                <c:out value="${student.index}"/>
            </td>
            <td>
                <c:out value="${student.birthdate}"/>
            </td>
                <td>
                    <table>
                        <tr>
                            <td>
                                <a href="remove_student.jsp?id=<c:out value="${student.id}"/>">Usun</a>
                                <%--<c:out value="${generujLinkUsuwania(student.id)}"/>--%>
                            </td>
                            <td>
                                <a href="student_form.jsp?id=<c:out value="${student.id}"/>">Edytuj</a>
                            </td>
                        </tr>
                    </table>

                </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>
