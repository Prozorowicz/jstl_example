<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Tomasz
  Date: 02/10/2018
  Time: 17:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Tabliczka Static</title>
</head>
<body>
<h2> tabliczka mnożenia static</h2>
<table>
<c:forEach var="i" begin="1" end="50">
    <tr>
    <c:forEach var="j" begin="1" end="50">
        <td>
    <c:out value="${i*j}"/>
        </td>
</c:forEach>
    </tr>
    </c:forEach>
</table>
</body>
</html>
